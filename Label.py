import os
import pickle
import logging

import global_settings as gs
from utils.io_utils import aggregate_raw_image_data
from utils.img_utils import estimate_wall_thicknesses, align_wall_thicknesses_to_phase_imgs, create_labels
from DataReader import DataReader

logger = logging.getLogger(gs.LOGGER_NAME)


class Label:
  def __init__(self, config):
    """
    Creates labeled data from the given config.
    :param Config.Config config:
    """
    # config parameters
    self.data_root_dir = config.value('data_dir', gs.DEFAULT_DATA_ROOT_DIR)
    self.output_dir = config.value('output_dir', gs.DEFAULT_OUTPUT_DIR)
    self.output_type = config.value('output_type', gs.DEFAULT_OUTPUT_TYPE)
    self.data_type = config.value('data_type', gs.DEFAULT_DATA_TYPE)
    self.create_binary_masks = config.value('create_binary_masks', False)
    self.keep_ct_stacks = config.value('keep_ct_stacks', False)
    self.gray_value_min_thickness = config.value('gray_value_min_thickness', 0)  #   0 =  0 mm
    self.gray_value_max_thickness = config.value('gray_value_max_thickness', 10) # 255 = 10 mm

    self.data = None
    self.create_labeled_data()

  def create_labeled_data(self):
    """
    Creates labeled data.
    :return: None
    """
    logger.info('Reading data')
    self.data = DataReader.aggregate_data(self.data_root_dir, self.data_type, self.create_binary_masks)
    logger.info('Creating labels')
    self.data.create_labels(self.gray_value_min_thickness, self.gray_value_max_thickness)
    logger.info('Writing data to file')
    if not self.keep_ct_stacks:
      delattr(self.data, 'ct_scans')
    self.data.write_to_file(self.output_dir, self.output_type)
    logger.info('Done')


  def validate_thickness_alignment(self):
    print('Calling validation')
    import matplotlib.pyplot as plt
    from copy import deepcopy
    N = self.data.N
    for i in range(N):

      phase_img = deepcopy(self.data.inputs[i][:,:,0])
      thickness = deepcopy(self.data.targets[i])

      thickness[thickness > 0] = 1
      accuracy = phase_img * thickness

      figure, axes = plt.subplots(3, 1)
      axes[0].imshow(phase_img)
      axes[1].imshow(self.data.targets[i])
      axes[2].imshow(accuracy)
      plt.show()
