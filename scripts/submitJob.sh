#!/usr/local_rwth/bin/zsh
# ask for 10 GB memory
#SBATCH --mem-per-cpu=10240M   #M is the default and can therefore be omitted, but could also be K(ilo)|G(iga)|T(era)
# name the job
#SBATCH --job-name=SERIAL_JOB
# declare the merged STDOUT/STDERR file
#SBATCH --output=output.%J.txt
# ask for 1 gpu
#SBATCH --gres=gpu:pascal:1
# time
#SBATCH -t 12:00:00
 
### begin of executable commands
/home/kk732748/venvs/3.6/bin/python train_mlp.py
