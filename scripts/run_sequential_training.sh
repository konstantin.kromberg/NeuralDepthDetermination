#!/usr/bin/env bash

helpFunction(){
  echo ""
  echo "Usage: $0 config_dir"
  echo -e "\tconfig_dir: a valid directory containing the config_files"
  exit 1
}


config_dir=$1
if [ ! -d $config_dir ]
then
    print "Directory '$config_dir' does not exist\nexit\n"
    helpFunction
fi

