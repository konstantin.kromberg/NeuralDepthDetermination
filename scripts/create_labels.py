import os
import inspect
import sys

CUR_DIR  = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
ROOT_DIR = os.path.dirname(CUR_DIR)
sys.path.insert(0, ROOT_DIR)
from Config import ConfigGenerator
from Label import Label

config_generator = ConfigGenerator('.', 'data', 'configs')
configs          = config_generator.generate_label_configs()

for config in configs:
  data_dir = config.value('data_dir', '')
  img_type = config.value('image_type', '')
  print('Creating labels for directory: %s and image type: %s' % (data_dir, img_type))
  Label(config)
