#!/usr/bin/env bash


config_dir=$1
if [ ! -d $config_dir ]
then
  echo "'$config_dir' is not a valid directory"
  exit
fi

# num_configs= ls -1q $config_dir | wc -w


for file in "$config_dir"/*.config
do
  echo $file
  python main.py "$file"
done

