import os
import inspect
import sys

CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
ROOT_DIR = os.path.dirname(CUR_DIR)
sys.path.insert(0, ROOT_DIR)
from Config import ConfigGenerator
from Train import Train

dataset_prefix = 'data/20-02-09_17_specimens'
num_layers = [2, 4, 6, 8, 10]
num_units = [16, 32, 64, 128, 256]
# losses = ['mae', 'mse', 'msle', 'cosine_similarity', 'logcosh', 'huber']
losses = ['mae']
data_types = ['phase']
input_shapes = [21, (512, 640, 21)]
network_types = ['lstm', 'conv2d']
# second argument can be the dataset_prefix
if len(sys.argv) == 2:
  dataset_prefix = sys.argv[1]


def generate_lstm_conv2_configs():
  output_dir = 'models/lstm_tuning_work'
  config_generator = ConfigGenerator(output_dir, 'models', 'configs')
  shuffle_specimens = [0, 3, 4, 5, 7, 9, 10, 12, 14, 15, 6, 8, 13, 1, 2, 11, 16]
  configs = config_generator.generate_train_configs(dataset_prefix, network_types=network_types, input_shapes=input_shapes,
                                                    dropout_rates=[None], test_split=0.25, val_split=0.25,
                                                    num_layers=num_layers, num_units=num_units,
                                                    shuffle_specimens=shuffle_specimens, img_types=data_types,
                                                    losses=losses, normalizations=['mean-variance'],
                                                    output_activation='relu', num_output_units=1)


generate_lstm_conv2_configs()
