#!/usr/bin/env bash

imgdir=$(realpath $1)
degree=$2
echo $imgdir

# avoid none file names
shopt -s nullglob
echo "Rotating images in $imgdir"
#for file in $imgdir/*.{bmp,tif,png};
for file in $imgdir/*.tif;
do
    convert "$file" -rotate $degree $imgdir/"$(basename "$file")" ;
done
shopt -u nullglob
echo "Done!"
