import keras
from keras import Sequential
from keras.layers import Dense

import cv2
import numpy as np

import global_settings as conf
from Dataset import load_data_from_pickle, get_data



# x_train, y_train = load_data_from_pickle('./data/labeled/imgs_with_labels_ThicknessLabeling_one_to_one.p')
x_train, y_train = get_data('data/raw_new')

# cv2.imshow('x', x_train[0, :, :, 0].astype(np.uint8))
# cv2.imshow('y', y_train[:, :])
# cv2.waitKey(0)
N = x_train.shape[0]
input_dim = conf.INPUT_DIM
x_train = x_train.reshape(N, -1, input_dim)
y_train = y_train.reshape(N, -1, 1)

x_train = x_train / 255
y_train = y_train / np.max(y_train)

p = np.random.permutation(x_train.shape[1])
x_train = x_train[:, p, :]
y_train = y_train[:, p, :]
print(x_train.shape)
print(y_train.shape)
model = Sequential()
# model.add(Dense(units=64, activation='relu', input_shape=x_train.shape[1:]))
model.add(Dense(units=64, activation='relu', input_shape=x_train.shape[1:]))
# model.add(Dense(units=128, activation='relu'))
model.add(Dense(units=1, kernel_initializer='normal'))


# model.compile(loss=keras.losses.mean_squared_error, optimizer='adam', metrics=['accuracy'])
# model.compile(loss=keras.losses.mean_absolute_percentage_error, optimizer='adam', metrics=['accuracy'])
model.compile(loss=keras.losses.mean_absolute_error, optimizer='nadam', metrics=['accuracy'])
# keras.optimizers

model.fit(x_train, y_train, epochs=100)
