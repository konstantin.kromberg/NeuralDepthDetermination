import os
import sys
import logging
import math
import glob
from pathlib import Path
import numpy as np
import cv2

from .generic_utils import min_max_norm

logger = logging.getLogger('NDD')


def create_phase_img_mask(auto_histogram_img_path):
  """
  Creates a binary mask for a given auto histogram image
  :param auto_histogram_img_path:
  :return: binary mask
  :rtype:
  """
  img = cv2.imread(auto_histogram_img_path, cv2.IMREAD_GRAYSCALE)
  ret, thresh = cv2.threshold(img, 190, 255, cv2.THRESH_BINARY_INV)
  mask = np.zeros(img.shape)
  cnts, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

  if len(cnts) > 0:
    largest_cnt_area = -1
    cnt_idx = -1
    for idx, c in enumerate(cnts):
      cnt_area = cv2.contourArea(c)
      if cnt_area > largest_cnt_area:
        largest_cnt_area = cnt_area
        cnt_idx = idx
    cv2.drawContours(mask, cnts, cnt_idx, (255, 255, 255), -1)
  else:
    print('writing thresh as mask')
    mask = thresh

  output_dir = Path(auto_histogram_img_path).parent.parent
  output_file = os.path.join(output_dir, 'mask.png')
  cv2.imwrite(output_file, mask)
  # cv2.imwrite(output_file, mask, cv2.IMWRITE_PNG_BILEVEL)


def create_ct_mask(ct_dir):
  """
  Creates a binary mask from
  :param ct_dir:
  :return:
  """
  ct_file_names = glob.glob(ct_dir + '/*.tif')
  ct_stack = []
  for ct_file_name in ct_file_names:
    ct_img = cv2.imread(ct_file_name, cv2.IMREAD_GRAYSCALE)
    ct_stack.append(ct_img)
  ct_stack = np.stack(ct_stack)
  # ct_stack[ct_stack > 50] = 1
  ct_stack = ct_stack.max(axis=0)

  # ret, thresh = cv2.threshold(ct_stack, 190, 255, cv2.THRESH_BINARY_INV)
  ret, thresh = cv2.threshold(ct_stack, 5, 200, cv2.THRESH_BINARY)
  mask = np.zeros(ct_stack.shape)
  cnts, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

  if len(cnts) > 0:
    largest_cnt_area = -1
    cnt_idx = -1
    for idx, c in enumerate(cnts):
      cnt_area = cv2.contourArea(c)
      if cnt_area > largest_cnt_area:
        largest_cnt_area = cnt_area
        cnt_idx = idx
    cv2.drawContours(mask, cnts, cnt_idx, (255, 255, 255), -1)
  else:
    print('writing thresh as mask')
    mask = thresh

  output_file = os.path.join(ct_dir, 'mask.png')
  cv2.imwrite(output_file, mask)
  # cv2.imwrite(output_file, mask, cv2.IMWRITE_PNG_BILEVEL)


def estimate_wall_thicknesses(N, ct_imgs, ct_img_masks, gray_value_min_thickness, gray_value_max_thickness,
                              voxel_size=0.09941698):
  """
  Estimates the wall thicknesses
  :param int N: number of specimens
  :param list[np.ndarray] ct_imgs:
  :param list[np.ndarray] ct_img_masks:
  :param int|float gray_value_min_thickness:
  :param int|float gray_value_max_thickness:
  :param float voxel_size:
  :return:
  """

  labels = []
  for n in range(N):
    num_images, _, _, = ct_imgs[n].shape
    mask = ct_img_masks[n]
    mask[mask > 0] = 1

    # old
    # ct_img = ct_imgs[n] * mask
    # mean = np.mean(ct_img)
    # ct_img[ct_img < mean] = 0
    # ct_img[ct_img >= mean] = 1
    # ct_img = np.sum(ct_img, axis=0)
    # label = ct_img * voxel_size

    # ct_imgs[n][ct_imgs[n] >= 240] = 0 # remove noise/ct contours
    ct_stack = (ct_imgs[n] * mask).astype(np.float64)
    ct_stack = min_max_norm(ct_stack, gray_value_min_thickness, gray_value_max_thickness, prev_min=0, prev_max=255)
    ct_stack[ct_stack > 8] = 0 # remove noise/ct contours
    label    = np.max(ct_stack, axis=0)

    labels.append(label)

    # import matplotlib.pyplot as plt
    # plt.imshow(label)
    # plt.show()

  return labels


def create_binary_labels(thicknesses):
  """
  Creates a binary labels for wall thicknesses which are below the mean wall thickness
  :param thicknesses:
  :return:
  """
  binary_labels = np.zeros_like(thicknesses, dtype=np.uint8)
  for n, t in enumerate(thicknesses.copy()):
    mean                    = np.mean(t)
    t[(t < mean) & (t > 0)] = 1
    t[t > 1]                = 0
    binary_labels[n]        = t

  return binary_labels


def create_labels(thicknesses, class_to_thickness):
  idx_to_class_and_thickness = {idx: c_and_t for idx, c_and_t in enumerate(class_to_thickness)}

  thicknesses = thicknesses.copy()
  labels      = np.zeros_like(thicknesses, np.uint8)

  t = 0
  for class_idx, c_and_t in enumerate(class_to_thickness):
    if isinstance(c_and_t, str):
      labels[thicknesses > t] = class_idx
    else:
      t = c_and_t[1]
      labels[(thicknesses <= t) & (thicknesses > -1)] = class_idx
      thicknesses[thicknesses <= t] = -1

  return labels, idx_to_class_and_thickness


def find_mask_corners(mask):
  """
  Finds the bounding box's corners for the given mask
  :param np.ndarray mask:
  :return:
  """

  rows = np.any(mask, axis=0)
  cols = np.any(mask, axis=1)
  y1, y2 = np.where(rows)[0][[0, -1]]
  x1, x2 = np.where(cols)[0][[0, -1]]

  return np.asarray([(y1, x1), (y1, x2), (y2, x1), (y2, x2)])


def align_wall_thicknesses_to_phase_imgs(N, thicknesses, phase_img_masks, ct_img_masks):
  """
  Aligns the labels to the phase images
  :param int N:
  :param list[np.ndarray] thicknesses:
  :param np.ndarray phase_img_masks:
  :param list[np.ndarray] ct_img_masks:
  :return:
  """
  diffs = 0
  total = 0
  for n in range(N):
    phase_img_corners = find_mask_corners(phase_img_masks[n])
    ct_img_corners    = find_mask_corners(ct_img_masks[n])

    H, W      = phase_img_masks[n].shape
    h, _      = cv2.findHomography(ct_img_corners, phase_img_corners, cv2.RANSAC)
    thicknesses[n] = cv2.warpPerspective(thicknesses[n], h, (W, H))

    # uncomment to visualize the mask transformation
    # visualize_transformation(phase_img_masks[n], phase_img_corners, ct_img_masks[n], ct_img_corners)

    # uncomment to compute alignment error
    # from copy import deepcopy
    # ct_m = cv2.warpPerspective(deepcopy(ct_img_masks[n]), h, (W, H)).flatten()
    # ct_m[ct_m > 0] = 1
    # p_m = deepcopy(phase_img_masks[n]).flatten()
    # p_m[p_m > 0] = 1
    # assert ct_m.shape == p_m.shape
    # total += p_m.shape[0]
    # diffs += np.sum(np.logical_xor(ct_m, p_m))
  if total > 0:
    # 0.007152243221507353
    print('Alignment error:', diffs/total)
  labels = np.asarray(thicknesses)

  return labels


def visualize_transformation(phase_mask, phase_mask_corners, ct_mask, ct_mask_corners):
  phase_mask[phase_mask > 0] = 255
  ct_mask[ct_mask > 0] = 255
  keypoints1 = []
  keypoints2 = []

  for idx in range(len(phase_mask_corners)):
    k1 = cv2.KeyPoint(phase_mask_corners[idx][0], phase_mask_corners[idx][1], 50)
    keypoints1.append(k1)
    k2 = cv2.KeyPoint(ct_mask_corners[idx][0], ct_mask_corners[idx][1], 50)
    keypoints2.append(k2)

  matches = []
  for idx in range(len(phase_mask_corners)):
    matches.append(cv2.DMatch(idx, idx, sys.float_info.max))

  output_img = np.empty((max(phase_mask.shape[0], ct_mask.shape[0]), phase_mask.shape[1] + ct_mask.shape[1], 3),
                        dtype=np.uint8)

  draw_params = dict(
    matchColor=(0, 255, 0),
    singlePointColor=(255, 0, 0),
    flags=4
  )
  cv2.drawMatches(phase_mask, keypoints1, ct_mask, keypoints2, matches, output_img, **draw_params)

  import matplotlib.pyplot as plt
  plt.imshow(output_img)
  plt.show()


def visualize_lateral_heat_flow(data, exc_freq_idx=16):
  """

  :param DataReader.Data data:
  :param int exc_freq_idx:
  :return:
  """
  import matplotlib.pyplot as plt
  for n in range(data.N):
    src_img = data.targets[n]
    target_img = data.inputs[n, :, :, exc_freq_idx]
    result, _ = segment_circles(src_img, target_img)

    # plt.title(n)
    plt.imshow(result, cmap='gray')
    plt.show()
    # plt.imshow(src_img)
    # plt.show()


def segment_circles(src_img, target_img, circle_color=(0, 255, 0), min_dist=60, param1=255, param2=16, min_radius=0,
                    max_radius=40):
  """
  Finds circles in the given image and draws them into the given image
  :param src_img:
  :param target_img:
  :param circle_color:
  :param min_dist:
  :param param1:
  :param param2:
  :param min_radius:
  :param max_radius:
  :return:
  """

  if src_img.dtype != np.uint8:
    src_img = min_max_norm(src_img, 0, 255, dtype=np.uint8)
  if target_img.dtype != np.uint8:
    target_img = min_max_norm(target_img, 0, 255, dtype=np.uint8)
  src_img    = cv2.medianBlur(src_img, 5)

  target_cimg = cv2.cvtColor(target_img, cv2.COLOR_GRAY2BGR)

  circles = cv2.HoughCircles(src_img, cv2.HOUGH_GRADIENT, 1, min_dist,
                             param1=param1, param2=param2, minRadius=min_radius, maxRadius=max_radius)

  if circles is not None:
    print('Found %s boreholes.' % circles.shape[1])
    circles = np.uint16(np.around(circles))

    # sort first by x then by y
    circles = circles[:, np.lexsort((circles[0, :, 0], circles[0, :, 1])), :]

    for i in circles[0, :]:
      # draw the outer circle
      cv2.circle(target_cimg, (i[0], i[1]), i[2], circle_color, 1, cv2.LINE_AA)
      # draw the center of the circle
      # cv2.circle(target_cimg, (i[0], i[1]), 2, (0, 0, 255), 3)
  else:
    circles = []


  return target_cimg, circles


def create_circular_mask(h, w, center=None, radius=None):
  # Source: https://stackoverflow.com/questions/44865023/circular-masking-an-image-in-python-using-numpy-arrays
  if center is None: # use the middle of the image
    center = [int(w/2), int(h/2)]
  if radius is None: # use the smallest distance between the center and image walls
    radius = min(center[0], center[1], w-center[0], h-center[1])

  Y, X = np.ogrid[:h, :w]
  dist_from_center = np.sqrt((X - center[0])**2 + (Y-center[1])**2)

  mask = dist_from_center <= radius
  return mask


def find_closest_circle_and_overlapping_area(x1, y1, r1, circles):
  # Source: https://stackoverflow.com/a/14646734/5882981
  x1    = float(x1)
  y1    = float(y1)
  r1    = float(r1)
  r1sqr = r1 ** 2
  c1_area = np.pi * r1sqr

  closest_circle      = None
  smallest_deviation = 1.0

  # no circles could be found in prediction
  if circles is not None and len(circles) > 0:

    for x2, y2, r2 in circles[0]:
      x2    = float(x2)
      y2    = float(y2)
      r2    = float(r2)
      r2sqr = r2 ** 2

      tmp_area  = -1.0
      deviation =  1.0
      c2_area   = np.pi * r2sqr
      # distance between the center points
      d = np.hypot(x1 - x2, y1 - y2)

      # circles do not overlap
      if d > (r1 + r2):
        pass
      # circle 2 is inside circle 1
      elif d <= abs(r1 - r2) and r1 >= r2:
        tmp_area = np.pi * r2sqr
        deviation = 1.0 - tmp_area / c1_area
      # circle 1 is inside circle 2
      elif d <= abs(r1 - r2) and r1 <= r2:
        tmp_area = np.pi * r1sqr
        # deviation = 1.0 - (np.pi * r1sqr) / (np.pi * r2sqr)
        deviation = 1.0 - tmp_area / c2_area
      # partial overlap
      else:
        phi   = (math.acos((r1sqr + (d * d) - r2sqr) / (2 * r1 * d))) * 2
        theta = (math.acos((r2sqr + (d * d) - r1sqr) / (2 * r2 * d))) * 2
        area1 = 0.5 * theta * r2sqr - 0.5 * r2sqr * math.sin(theta)
        area2 = 0.5 *   phi * r1sqr - 0.5 * r1sqr * math.sin(phi)
        tmp_area = area1 + area2

        deviation = 1.0 - tmp_area / (c1_area + c2_area - tmp_area)

      # deviation = max(abs(np.pi * r1sqr - tmp_area), abs(np.pi * r1sqr - np.pi * r2sqr)) # works

      # deviation = max(1 - (np.pi * r2sqr) / (np.pi * r1sqr),  1 - (np.pi * r1sqr) / (np.pi * r2sqr))

      if tmp_area > -1 and deviation < smallest_deviation:
        smallest_deviation = deviation
        closest_circle      = [int(x2), int(y2), int(r2)]

  # return closest_circle, overlapping_area
  return closest_circle, smallest_deviation


def find_convex_hulls(img, largest_contour_area=4000):
  import matplotlib.pyplot as plt
  if img.dtype != np.uint8:
    img = min_max_norm(img, 0, 255, dtype=np.uint8)

  # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  # convert to grayscale
  blur = cv2.blur(img, (3, 3))  # blur the image
  ret, thresh = cv2.threshold(blur, 120, 255, cv2.THRESH_BINARY)
  cnts, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

  # draw contours
  img2 = img.copy()
  for c in cnts:
    c_area = cv2.contourArea(c)
    if c_area < largest_contour_area:
      M = cv2.moments(c)
      c_x = int(M['m10'] / M['m00'])
      c_y = int(M['m01'] / M['m00'])
      cv2.drawContours(img2, [c], -1, (0, 255, 0), 2)
      cv2.circle(img2, (c_x, c_y), 7, (255, 255, 255), -1)
      cv2.putText(img2, 'center', (c_x - 20, c_y - 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)

  cv2.imshow('image', img2)
  cv2.waitKey(0)

  # plot result
  fig, axes = plt.subplots(1, 2, figsize=(8, 4))
  ax = axes.ravel()
  ax[0].set_title('Original')
  ax[0].imshow(img, cmap='gray')
  ax[0].set_axis_off()

  ax[1].set_title('Transformed')
  ax[1].imshow(img2, cmap='gray')
  ax[1].set_axis_off()

  plt.tight_layout()
  plt.show()


def create_stacked_image(imgs, offset_x=10, offset_y=50):
  """
  Create a shifted image stack from the given images
  :param numpy.array_like imgs: with shape (H, W, C)
  :param int offset_x: offset in width
  :param int offset_y: offset in height
  :return:
  """
  height, width, num_images = imgs.shape
  # single pixel has to be reshaped into (2,2) s.t a shift in x and y is possible
  if height == 1 and width == 1:
    height +=  1
    width  +=  1
    imgs    = np.full((height, width, num_images), imgs[0, 0, :])
  stacked_height = (num_images - 1) * offset_y + height
  stacked_width  = (num_images - 1) * offset_x + width
  stacked_image  = np.full((stacked_height, stacked_width), 255)
  j = 0
  for i in range(num_images - 1, -1, -1):
    for row in range(height):
      for col in range(width):
        stacked_image[j * offset_y + row, j * offset_x + col] = imgs[row, col, i]
    j += 1

  return stacked_image

