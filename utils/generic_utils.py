from __future__ import print_function

import os

import numpy as np

hd_print = lambda msg: print('\n' + msg, '\n' + '-' * len(msg.expandtabs()))


def hd_print2(msg):
  return msg + '\n' + '=' * len(msg.expandtabs()) + '\n'


def get_sub_dirs_and_files(dir):
  """
  Returns sub directories and files for the given directory in sorted order
  :param str dir: a directory
  :return:
  """
  assert os.path.isdir(dir), 'Directory "%s" does not exist' % dir
  for _, dirnames, filenames in os.walk(dir):
    return sorted(dirnames), sorted(filenames)


def get_all_filenames(dir, file_exts, recursive=False, ignored_folders=(), ignore_imgs_with=''):
  """

  :param str dir: folder path
  :param list|str file_exts: file extension(s) to consider
  :param boolean recursive:
  :param str ignored_folders:
  :param str ignore_imgs_with:
  :return:
  """

  f = []
  for (dirpath, dirnames, filenames) in os.walk(dir):
    if any(ignored_folder in dirpath for ignored_folder in ignored_folders):
      continue
    if len(filenames) > 0:
      img_names = list(filter(lambda x: True if x.endswith(file_exts) else False, filenames))
      if len(ignore_imgs_with) > 0:
        img_names = list(filter(lambda x: True if ignore_imgs_with.lower() not in x.lower() else False, img_names))
      img_paths = list(map(lambda x: dirpath + os.path.sep + x, img_names))

      if len(img_paths) > 0:
        f.extend(img_paths)

    if not recursive:
      break
  f = sorted(f)

  return f


def extract_exc_freqs(data_type, filenames, exc_freqs, idx_to_exc_freq):
  """
  :param str data_type: 'auto_histogram', 'full_scale' or 'phase'
  :param list filenames:
  :param list exc_freqs:
  :param dict[int] idx_to_exc_freq:
  :return:
  """
  import re
  from collections import OrderedDict

  found_freqs = set()
  tmp_freq_to_img_path = OrderedDict()
  if data_type == 'phase':
    pattern = r'\([0-9]+\)'
  else:
    pattern = r'\([0-9]*,[0-9]*Hz\)'

  for f in filenames:
    match = re.search(pattern, f)
    if match and len(match[0]) > 0:
      if data_type == 'phase':
        exc_idx = int(match[0][1:-1])
        exc_freq = idx_to_exc_freq[exc_idx]
      else:
        exc_freq = float(match[0][1:-3].replace(',', '.'))

      if exc_freq in exc_freqs:
        found_freqs.add(exc_freq)
        # found_freqs.append(exc_freq)
        tmp_freq_to_img_path[exc_freq] = f

  found_freqs = sorted(found_freqs, reverse=True)
  freq_to_img_path = OrderedDict()
  for freq in found_freqs:
    freq_to_img_path[freq] = tmp_freq_to_img_path[freq]

  return freq_to_img_path, found_freqs


def compute_measuring_time_per_specimen(exc_freqs, transient_periods=3, measurement_periods=3):
  """
  Computes the time needed to capture the phase images for a single specimen
  :param list exc_freqs:
  :param int transient_periods:
  :param int measurement_periods:
  :return:
  """
  _sum = 0
  for exc_freq in exc_freqs:
    _sum += exc_freq ** (-1)

  result = (_sum * (transient_periods + measurement_periods)) / 60
  return result


def compute_statistics(x):
  """
  Computes statistics like min, max, max, median and variance on x
  :param x:
  :return:
  """
  if x is None or len(x) == 0:
    return [-1, -1, -1, -1, -1]

  assert isinstance(x, np.ndarray)
  _min    = np.min(x)
  _max    = np.max(x)
  _median = np.median(x)
  _mean   = np.mean(x)
  _var    = np.var(x)

  return [_min, _max, _median, _mean, _var]


def min_max_norm(img, new_min=0, new_max=1, prev_min=None, prev_max=None, dtype=None):
  assert new_max > new_min
  if prev_min is not None:
    _min = prev_min
  else:
    _min = img.min()
  if prev_max is not None:
    _max = prev_max
  else:
    _max = img.max()
  if (_max == 0 and _min == 0) or (_max - _min) == 0:
    if dtype is not None:
      return img.astype(dtype)
    return img
  norm_img = new_min + ((img - _min) * (new_max - new_min)) / (_max - _min)

  if dtype:
    norm_img = norm_img.astype(dtype)

  return norm_img


def determine_ticks(start, stop):
  diff = abs(start - stop)
  step = 1
  if diff > 10000:
    step = 200
  elif diff > 5000:
    step = 100
  elif diff > 1000:
    step = 50

  ticks = np.arange(start, stop, step)

  return ticks
