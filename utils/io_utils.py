import os
import json
import cv2
import numpy as np

from .generic_utils import get_sub_dirs_and_files, get_all_filenames, extract_exc_freqs


def aggregate_raw_image_data(data_root_dir, img_type, gs, rgb2gray=True):
  """
  Aggregates raw image data including phase images, ct scans, masks, the total number of specimens,
  the absolute specimen paths and the excitation frequencies which were used to acquire the phase images.
  :param str data_root_dir:
  :param str img_type: the sub folder where the images are contained e.g. auto-histogram
  :param gs: global settings
  :param boolean rgb2gray: whether to convert RGB to grayscale
  :rtype: dict[str]
  """
  assert os.path.isdir(data_root_dir), 'Data root dir "%s" does not exist' % data_root_dir
  tmp_specimens, _ = get_sub_dirs_and_files(data_root_dir)
  N                = len(tmp_specimens)
  specimens        = []
  specimen_paths   = []
  phase_imgs       = np.zeros((N, gs.H, gs.W) + (gs.C,), dtype=np.uint8)
  phase_img_masks  = np.zeros((N, gs.H, gs.W), dtype=np.uint8)
  ct_imgs          = []
  ct_img_masks     = []

  # loop specimens
  for n, specimen in enumerate(tmp_specimens):
    img_stack = np.zeros((gs.H, gs.W) + (gs.C,), dtype=np.uint8)
    img_dir   = os.path.join(data_root_dir, specimen, img_type)
    assert os.path.isdir(img_dir)
    filenames = get_all_filenames(img_dir, gs.FILE_EXTS)
    freq_to_path, freqs = extract_exc_freqs(filenames, gs.EXC_FREQS)

    # found valid specimen
    if freqs == gs.EXC_FREQS:
      specimens.append(specimen)
      # store absolute specimen path
      specimen_path     = os.path.join(data_root_dir, specimen)
      specimen_path_abs = os.path.abspath(specimen_path)
      specimen_paths.append(specimen_path_abs)

      # read phase images
      for idx, [freq, img_path] in enumerate(freq_to_path.items()):
        f = open(img_path, 'rb')
        b = bytearray(f.read())
        np_array = np.asarray(b, dtype=np.uint8)
        if rgb2gray:
          img = cv2.imdecode(np_array, cv2.IMREAD_GRAYSCALE)
        else:
          # TODO: handle RGB colors later mb
          img = cv2.imdecode(np_array, cv2.IMREAD_COLOR)
        f.close()

        # rescale image
        if img.shape != gs.IMG_SHAPE:
          img = cv2.resize(img, gs.IMG_SHAPE)

        if gs.EXC_FREQS_AS_INPUT:
          # TODO: add freq as input
          pass
        img_stack[:, :, idx] = img

      phase_imgs[n, :, :, :] = img_stack

      # read phase image mask
      fn = os.path.join(specimen_path, 'mask.png')
      mask = cv2.imread(fn, cv2.IMREAD_GRAYSCALE)
      phase_img_masks[n] = mask

      # read CT images
      ct_stack  = []
      label_dir = 'CT'
      ct_dir    = os.path.join(specimen_path, label_dir)

      if os.path.isdir(ct_dir):
        ct_files = get_all_filenames(ct_dir, gs.FILE_EXTS, ignore_imgs_with='mask')
        for ct_path in ct_files:
          ct_img = cv2.imread(ct_path, cv2.IMREAD_GRAYSCALE)
          ct_stack.append(ct_img)
        if len(ct_stack) > 0:
          ct_imgs.append(np.stack(ct_stack))       # (C, H, W)
          # ct_imgs.append(np.stack(ct_stack, axis=2))  # (H, W, C)
        fn = os.path.join(ct_dir, 'mask.png')
        if os.path.isfile(fn):
          ct_mask = cv2.imread(fn, cv2.IMREAD_GRAYSCALE)
          ct_img_masks.append(ct_mask)

  # remove incomplete data
  num_dels = 0
  for n in range(N):
    # no phase image for specimen n was found
    if phase_imgs[n, :, :, :].sum() == 0 or ct_imgs[n][:, :, :].sum() == 0:
      specimens.pop(n)
      specimen_paths.pop(n)
      phase_imgs = np.delete(phase_imgs, n, 0)
      phase_img_masks = np.delete(phase_img_masks, n, 0)
      ct_imgs.pop(n)
      ct_img_masks.pop(n)
      num_dels += 1

  N = N - num_dels
  specimens      = np.array(specimens)
  specimen_paths = np.array(specimen_paths)

  data = {
    'num_specimens'         : N,
    'specimen_names'        : specimens,
    'specimen_paths'        : specimen_paths,
    'excitation_frequencies': gs.EXC_FREQS,
    'phase_imgs'            : phase_imgs,
    'phase_img_masks'       : phase_img_masks,
    'ct_imgs'               : ct_imgs,
    'ct_img_masks'          : ct_img_masks
  }

  return data
