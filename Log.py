import os
import logging
import time

import global_settings as gs

# class Log:
#   def __init__(self):
#     self.initialized   = False
#     self.output_dir    = None
#     self.logger_file   = None
#     self.log_formatter = logging.Formatter('%(asctime)s -  %(levelname)s - %(module)s -  %(message)s')
#
#   def init_from_config(self, config):
#     task       = config.value('task', 'train')
#     self.output_dir = config.value('output_dir', None)
#     if self.output_dir is None:
#       if task == 'segment':
#         pass
#       elif task == 'label':
#         self.output_dir = 'data'
#       elif task == 'train':
#         self.output_dir = os.path.join('models', config.filename)
#       elif task == 'evaluate':
#         pass
#       elif task == 'sever':
#         pass
#     if not os.path.isdir(self.output_dir):
#       os.mkdir(self.output_dir)
#     now = time.strftime('%Y_%m_%d_%H_%M_%S')
#     self.logger_file = os.path.join(self.output_dir, ('%s_%s.log' % (task, now)))
#
#
# log = Log()

logger_initialized = False


def init_logger(config):
  # output directory
  global logger_initialized
  if not logger_initialized:
    output_dir = config.value('output_dir', None)
    task = config.value('task', gs.DEFAULT_TASK)
    if output_dir is None:
      output_dir = gs.DEFAULT_OUTPUT_DIR
      if task == 'train':
        output_dir = os.path.join('models', config.filename)
      elif task == 'evaluate':
        pass
      elif task == 'serve':
        pass
    if not os.path.isdir(output_dir):
      os.makedirs(output_dir)
    # format
    logger = logging.getLogger(gs.LOGGER_NAME)
    logger.setLevel(logging.DEBUG)

    log_formatter = logging.Formatter('%(asctime)s -  %(levelname)s - %(module)s -  %(message)s')

    now          = time.strftime('%Y_%m_%d_%H_%M_%S')
    logger_file  = os.path.join(output_dir, ('%s_%s.log' % (task, now)))
    file_handler = logging.FileHandler(logger_file)
    file_handler.setFormatter(log_formatter)
    logger.addHandler(file_handler)

    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(log_formatter)
    logger.addHandler(stream_handler)

    logger_initialized = True
    return logger
