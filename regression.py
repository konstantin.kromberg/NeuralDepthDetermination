import os
import pickle
import time
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

import global_settings as gs
from Config import Config
from Dataset import Dataset
from utils import plot_utils
from utils.generic_utils import hd_print

"""
Load data
"""
conf_params = {
    'data_path' : 'data/08-11-19_data.pickle',
    'test_split': 0.3,
    'val_split' : 0.3,
    # 'input_shape':
    'shuffle_specimens': [0, 2, 1]
}
config = Config(filename=None, params=conf_params)

d = Dataset(config)
hd_print('Shape info')
d.shape_info()


class RegressionTrainer:

  def __init__(self, output_dir, dataset):
    assert isinstance(output_dir, str) and len(output_dir) > 0
    if not os.path.isdir(output_dir):
      os.mkdir(output_dir)
    self.output_dir = output_dir
    self.X_train    = dataset.X_train
    self.Y_train    = dataset.Y_train
    self.X_val      = dataset.X_val
    self.Y_val      = dataset.Y_val
    self.X_test     = dataset.X_test
    self.Y_test     = dataset.Y_test

  def train_model(self, config, model, model_name):
    start = time.time()
    print('Training %s' % model_name)
    model_output_dir = os.path.join(self.output_dir, model_name)
    model_path       = os.path.join(model_output_dir, 'model')

    if RegressionTrainer.is_already_trained(config, model_output_dir):
      try:
        f = open(model_path, 'rb')
        model = pickle.load(f)
        print(model.get_params())
        f.close()
        return model
      except FileNotFoundError:
        pass


    if not os.path.isdir(model_output_dir):
      os.mkdir(model_output_dir)
    model.fit(self.X_train, self.Y_train)
    RegressionTrainer.write_num_model_params_to_file(model, model_output_dir)

    f = open(model_path, 'wb')
    pickle.dump(model, f)
    f.close()
    end = time.time()
    elapsed = end - start
    print('Elapsed time for training: %d seconds' % elapsed)
    return model


  def evaluate_model(self, model, model_name):
    print('Evaluating %s' % model_name)
    start = time.time()
    model_output_dir = os.path.join(self.output_dir, model_name)
    if not os.path.isdir(model_output_dir):
      os.mkdir(model_output_dir)

    y      = self.Y_test.reshape(-1, gs.H, gs.W)
    y_pred = model.predict(self.X_test)
    y_pred = y_pred.reshape(-1, gs.H, gs.W)
    assert y.shape == y_pred.shape
    for n, _ in enumerate(y):
      plot_utils.plot_ground_truth_vs_prediction(            y, y_pred, gs, output_dir=model_output_dir, suffix=n)
      plot_utils.plot_ground_truth_vs_prediction_flattened(  y, y_pred,     output_dir=model_output_dir, suffix=n)
      plot_utils.plot_errors_per_bin(                        y, y_pred,     output_dir=model_output_dir, suffix=n, metrics=['mae'])
      plot_utils.plot_ground_truth_and_prediction_histograms(y, y_pred,     output_dir=model_output_dir, suffix=n)
      # plot_utils.plot_
    end = time.time()
    elapsed = end - start
    print('Elapsed time for evaluation: %d seconds' % elapsed)

  @staticmethod
  def write_num_model_params_to_file(model, model_output_dir):
    num_params = 0
    if hasattr(model, 'coef_'):
      num_params += len(model.coef_)
    if hasattr(model, 'intercept_'):
      print('Intercept:', model.intercept_)
      if isinstance(model.intercept_, list):
        num_params += len(model.intercept_)
      else:
        num_params += 1
    if hasattr(model, 'sigma_'):
      num_params += len(model.sigma_)
    # if hasattr(model, '')
    # write params to file
    if not os.path.isdir(model_output_dir):
      os.mkdir(model_output_dir)
    filename = os.path.join(model_output_dir, 'num_params.txt')
    f = open(filename, 'w')
    f.write(str(num_params))
    f.close()

  @staticmethod
  def is_already_trained(config, model_output_dir):
    conf_name = '.config'
    conf_path = os.path.join(model_output_dir, conf_name)
    if not os.path.isfile(conf_path):
      if not os.path.isdir(model_output_dir):
        os.mkdir(model_output_dir)
      f = open(conf_path, 'wb')
      pickle.dump(config, f)
      f.close()
      return False
    f = open(conf_path, 'rb')
    old_conf = pickle.load(f)
    f.close()
    if old_conf != config:
      f = open(conf_path, 'wb')
      pickle.dump(config, f)
      f.close()
      return False
    return True


  def compare_neural_versus_regression(self, neural_model_path, neural_model_name, regression_model_path, regression_model_name):
    import tensorflow as tf
    from keras.models import load_model
    neural_model = tf.keras.models.load_model(neural_model_path)
    y_pred_neural = neural_model.predict(self.X_test)
    print(y_pred_neural)
    f = open(regression_model_path, 'rb')
    reg_model = pickle.load(f)
    f.close()
    y_pred_reg = reg_model.predict(self.X_test)
    print(y_pred_reg)



reg_trainer = RegressionTrainer('models/regression', d)

"""
Ordinary Least Squares OLS
"""
from sklearn import linear_model
# model         = linear_model.LinearRegression(verbose=3)
# model_name    = 'OLS'
# trained_model = reg_trainer.train_model(config, model, model_name)
# reg_trainer.evaluate_model(trained_model, model_name)

"""
Bayesian Ridge Regression
"""
# model         = linear_model.BayesianRidge()
# model_name    = 'BayesianRidge'
# trained_model = reg_trainer.train_model(config, model, model_name, verbose=3)
# reg_trainer.evaluate_model(trained_model, model_name)


"""
Decision tree
"""
from sklearn import tree
# model        = tree.DecisionTreeRegressor(criterion='mae', presort=True, verbose=3)
# model_name    = 'DecisionTree'
# trained_model = reg_trainer.train_model(config, model, model_name)
# reg_trainer.evaluate_model(trained_model, model_name)
"""
SVR
"""
from sklearn.svm import SVR
# model         = SVR(gamma='auto', C=1.0, epsilon=0.6, verbose=3, cache_size=7000, max_iter=100)
# model_name    = 'SVR'
# trained_model = reg_trainer.train_model(config, model, model_name)
# reg_trainer.evaluate_model(trained_model, model_name)


"""
Random Forest Regressor
"""
from sklearn.ensemble import RandomForestRegressor
model         = RandomForestRegressor(criterion='mae', random_state=0, n_jobs=100, verbose=3)
model_name    = 'RandomForestRegressor'
trained_model = reg_trainer.train_model(config, model, model_name)
reg_trainer.evaluate_model(trained_model, model_name)

"""
Gradient Boosting Regressor
"""
from sklearn.ensemble import GradientBoostingRegressor
model         = GradientBoostingRegressor(loss='lad', criterion='mae', random_state=0, n_iter_no_change=10, verbose=3)
model_name    = 'GradientBoostingRegressor'
trained_model = reg_trainer.train_model(config, model, model_name)
reg_trainer.evaluate_model(trained_model, model_name)

"""
Bagging Regressor
"""
from sklearn.ensemble import BaggingRegressor
from sklearn.neighbors import KNeighborsRegressor
model         = BaggingRegressor(KNeighborsRegressor(), n_jobs=100, random_state=0, verbose=3)
model_name    = 'BaggingRegressorKNeighbourRegressor'
trained_model = reg_trainer.train_model(config, model, model_name)
reg_trainer.evaluate_model(trained_model, model_name)

# """
# Compare neural versus regression model
# """
# neural_model_path = 'models/train_FF_2x512.config/best_model.h5'
# neural_model_name = 'FF_2x512'
# reg_model_path    = 'models/regression/OLS/model'
# reg_model_name    = 'OLS'
# reg_trainer.compare_neural_versus_regression(neural_model_path, neural_model_name, reg_model_path, reg_model_name)

