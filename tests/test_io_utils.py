import os
import inspect
import sys
import unittest

CUR_DIR  = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
ROOT_DIR = os.path.dirname(CUR_DIR)
sys.path.insert(0, ROOT_DIR)

from utils.io_utils import aggregate_raw_image_data
import global_settings as gs


class TestIoUtils(unittest.TestCase):
  data_root_dir = os.path.join(ROOT_DIR, 'data/raw')

  def test_aggregate_raw_image_data_auto_histogram(self):
    data = aggregate_raw_image_data(self.data_root_dir, 'auto_histogram', gs)
    self.assertNotEqual(data, None)
    self.check_num_specimens(data)

  def test_aggregate_raw_image_data_full_histogram(self):
    data = aggregate_raw_image_data(self.data_root_dir, 'full_histogram', gs)
    self.assertNotEqual(data, None)
    self.check_num_specimens(data)

  def check_num_specimens(self, data):
    N = data['num_specimens']

    specimens = data['specimen_names']
    self.assertEqual(N, len(specimens))

    specimen_paths = data['specimen_paths']
    self.assertEqual(N, len(specimen_paths))

    exc_freqs = data['excitation_frequencies']
    self.assertEqual(gs.EXC_FREQS, exc_freqs)

    phase_imgs = data['phase_imgs']
    self.assertEqual(N, phase_imgs.shape[0])

    phase_img_masks = data['phase_img_masks']
    self.assertEqual(N, phase_img_masks.shape[0])

    ct_imgs = data['ct_imgs']
    self.assertEqual(N, len(ct_imgs))

    ct_img_masks = data['ct_img_masks']
    self.assertEqual(N, len(ct_img_masks))


if __name__ == '__main__':
  unittest.main()
