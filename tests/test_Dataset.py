import os
import inspect
import sys
import unittest

CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
ROOT_DIR = os.path.dirname(CUR_DIR)
sys.path.insert(0, ROOT_DIR)
from Dataset import Dataset


class TestRandomDataset(unittest.TestCase):
  dataset = Dataset()
  N = 100
  H = 512
  W = 512
  test_split = 0.3
  val_split = 0.3
  C = 7

  X_shape = (N, H, W, C)
  Y_shape = (N, H, W)

  def setUp(self):
    pass

  def tearDown(self):
    pass

  def test_input_shapes(self):
    self.dataset.init_dataset_random(self.N, self.H, self.W, self.C, self.C, self.test_split, self.val_split)
    X, Y, Y_label = self.dataset.original_data

    self.assertEqual(X.shape, self.X_shape)
    self.assertEqual(Y.shape, self.Y_shape)
    self.assertEqual(Y_label.shape, self.Y_shape)

    input_shapes = [3, (self.H, self.W, self.C), (self.H, self.W, 5)]
    for input_shape in input_shapes:
      # TODO: asserts
      self.dataset.init_dataset_random(self.N, self.H, self.W, self.C, input_shape, self.test_split, self.val_split)


if __name__ == '__main__':
  unittest.main()
