import os
import inspect
import sys
import unittest
import tempfile
import shutil

CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
ROOT_DIR = os.path.dirname(CUR_DIR)
sys.path.insert(0, ROOT_DIR)
from Config import ConfigGenerator
from Train import Train


class TestTrainConfigs(unittest.TestCase):
  # data_path_prefix = 'data/16-12-19_17_specimens'
  data_path_prefix = 'data/16-12-19_3_specimens'

  def setUp(self):
    self.root_dir = tempfile.mkdtemp()
    self.config_generator = ConfigGenerator(self.root_dir, output_dir='models', config_dir='configs')
    print('Root dir:', self.root_dir)

  def tearDown(self):
    pass
    # shutil.rmtree(self.output_dir)

  def test_train(self):
    # configs = self.config_generator.generate_train_configs(self.data_path_prefix, num_epochs=1, img_types=['auto_histogram'],
    #                                                        normalizations=[None],
    #                                                        #network_types=['ff', 'simple_rnn'],
    #                                                        shuffle_pixels=[False])
    #
    # for config in configs:
    #   print('Testing train config with input shape: ', config.value('input_shape'), '\n for config: ', config.filename)
    #   Train(config)

    configs = self.config_generator.generate_train_configs(self.data_path_prefix, num_epochs=1, normalizations=[None],
                                                           img_types=['auto_histogram'], dropout_rates=[None],
                                                           num_layers=[2, 4, 8], num_units=[16, 32, 64, 128], shuffle_pixels=[False])

    for config in configs:
      print('Testing train config with input shape: ', config.value('input_shape'), '\n for config: ', config.filename)
      Train(config)


if __name__ == '__main__':
  unittest.main()
