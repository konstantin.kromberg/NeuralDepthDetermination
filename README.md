# Neural Depth Determination
Neural Depth Determination is a tool that provides the possibilities to:

* create labeled datasets
* build and train models
* deploy the trained models

An overview of the general pipeline is illustrated in the image below.</br>
<!--![alt text](./misc/pipeline.png)-->
<p align="center">
<img alt="Pipeline overview" src="./misc/pipeline.png"/>
</p> 

## Getting Started
Make sure that all [requirements](./requirements.txt) are satisfied and available to your python interpreter.<br>
To setup a new python environment following steps are recommended:<br>
1. set up and activate a python virtual environment
```console
$ cd NeuralDepthDetermination
$ python -m venv my_venv
$ source my_venv/bin/activate
```
2. install requirements 
```console
(my_venv)$ pip install -r requirements.txt
```

## Usage
The entry point of the tool is the [main.py](./main.py) which expects configuration file as an additional argument.
```console
$ python main.py my_config.config
```
The config file has to be provided in python syntax and are of the form key=value pairs and specifies what <b>task</b> will be executed.<br>
Possible tasks are: segment, label, train and serve.
Their implementation is in the modules [Segment.py](./Segment.py), [Label.py](./Label.py), [Train.py](./Train.py) and
[Serve.py](./Serve.py) respectively.<br>
Demo configuration files are available under [demos](./demos) and can be investigated for further information and
concrete usage.

## Project Structure
The project structure with the main modules is depicted below.
```
NeuralDepthDetermination
├── data
├── demos
├── scripts
├── tests
├── utils
├── Config.py
├── Dataset.py
├── Evaluate.py
├── global_settings.py
├── Label.py
├── Log.py
├── main.py
├── Segment.py
├── Serve.py
└── Train.py
```
### Config.py
Provides methods to read a configuration file in key python syntax

## Label
In order to create a labeled dataset the folder structure is expected to follow the format below.<br>
For further    
```
NeuralDepthDetermination/data/raw_640x512
├── 111a
│   ├── mask.png
│   ├── auto_histogram
│   │   ├── wlh_kk_CFRP_640x512_111a\ [Phase\ (°)]\ (0,005Hz).bmp
│   │   ├── wlh_kk_CFRP_640x512_111a\ [Phase\ (°)]\ (0,020Hz).bmp
│   │   ├── wlh_kk_CFRP_640x512_111a\ [Phase\ (°)]\ (0,023Hz).bmp
|   .   .
|   .   .
|   .   .
│   │   ├── wlh_kk_CFRP_640x512_111a\ [Phase\ (°)]\ (0,300Hz).bmp
│   │   ├── wlh_kk_CFRP_640x512_111a\ [Phase\ (°)]\ (0,400Hz).bmp
│   │   └── wlh_kk_CFRP_640x512_111a\ [Phase\ (°)]\ (0,500Hz).bmp
│   ├── CT
│   │   ├── mask.png
│   │   ├── XY00.tif
│   │   ├── XY01.tif
│   │   ├── XY02.tif
|   .   .
|   .   .
|   .   .
│   │   ├── XY75.tif
│   │   ├── XY76.tif
│   │   └── XY77.tif
│   ├── full_scale
│   │   ├── wlh_kk_CFRP_640x512_111a\ [Phase\ (°)]\ (0,005Hz).bmp
│   │   ├── wlh_kk_CFRP_640x512_111a\ [Phase\ (°)]\ (0,020Hz).bmp
│   │   ├── wlh_kk_CFRP_640x512_111a\ [Phase\ (°)]\ (0,023Hz).bmp
|   .   .
|   .   .
|   .   .
│   │   ├── wlh_kk_CFRP_640x512_111a\ [Phase\ (°)]\ (0,300Hz).bmp
│   │   ├── wlh_kk_CFRP_640x512_111a\ [Phase\ (°)]\ (0,400Hz).bmp
│   │   └── wlh_kk_CFRP_640x512_111a\ [Phase\ (°)]\ (0,500Hz).bmp
│   └── phase
│       ├── wlh_kk_CFRP_640x512_111a\ [Phase\ (°)]\ (01).tiff
│       ├── wlh_kk_CFRP_640x512_111a\ [Phase\ (°)]\ (02).tiff
│       ├── wlh_kk_CFRP_640x512_111a\ [Phase\ (°)]\ (03).tiff
|       .
|       .
|       .
│       ├── wlh_kk_CFRP_640x512_111a\ [Phase\ (°)]\ (19).tiff
│       ├── wlh_kk_CFRP_640x512_111a\ [Phase\ (°)]\ (20).tiff
│       └── wlh_kk_CFRP_640x512_111a\ [Phase\ (°)]\ (21).tiff
├── 111b
.   .
.   .
.   .
```
#### Phase Images
Each specimen 111a, 111b, etc. can be represented in different image types, namely
```auto_histogram, full_scale or phase```.
The ```img_type``` can be specified via the configuration file and is expected to be a sub folder for each specimen.<br>
Furthermore, a binary ```mask.png``` is expected to be in the root folder of each specimen.
This binary mask can also be created automatically by specifying the parameter ```create_binary_masks=True```.
To achieve a more accurate labeling an interactive segmentation tool is recommended
(see [InteractiveSegmentationTool](./misc/interactive_segmentation_tool)).


#### CT
To create the labels a CT folder is also to be expected for each specimen containing a binary
mask and the exported CT-scans.
The binary mask can be again created using the interactive segmentation tool mentioned above.

## Train
## Evaluate
## Deploy

## TODOs
* deploy methods
* evaluate model only(evaluations works so far only after training) 
* improve automatic segmentation (binary masks for phase images and CT-scans)
* optional: cache images to avoid multiple reading of images for labeling
