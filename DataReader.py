import os
import glob
import logging
import cv2
import numpy as np

import global_settings as gs
from utils.generic_utils import get_sub_dirs_and_files, get_all_filenames, extract_exc_freqs
from utils.img_utils import create_phase_img_mask, create_ct_mask, estimate_wall_thicknesses, \
  align_wall_thicknesses_to_phase_imgs, visualize_lateral_heat_flow

logger = logging.getLogger(gs.LOGGER_NAME)


class Data:
  """Stores all information for each specimen"""

  def __init__(self):
    self.N = 0
    self.H = gs.H
    self.W = gs.W
    self.C = gs.C
    self.specimen_names = []
    self.specimen_paths = []
    self.excitation_frequencies = gs.EXC_FREQS

    self.ct_scans = None
    # masks
    self.auto_histogram_img_masks = None
    self.ct_scan_masks = None

    self.specimen_idx_to_name = {}

    self.data_type = None
    self.inputs = None
    self.targets = None

    # create missing masks?
    # estimate thicknesses
    # map labels to data

  def create_labels(self, gray_value_min_thickness=0, gray_value_max_thickness=10):
    self.specimen_idx_to_name = {idx: name for idx, name in enumerate(self.specimen_names)}
    thicknesses = estimate_wall_thicknesses(self.N, self.ct_scans, self.ct_scan_masks,
                                            gray_value_min_thickness, gray_value_max_thickness)
    thicknesses = align_wall_thicknesses_to_phase_imgs(self.N, thicknesses, self.auto_histogram_img_masks,
                                                       self.ct_scan_masks)

    self.targets = thicknesses
    # uncomment to visualize lateral heat flow or the mapping of defects
    # visualize_lateral_heat_flow(self, exc_freq_idx=16)


  def write_to_file(self, output_dir, output_type='pickle'):
    # delete ct_scans (and binary masks?)
    from datetime import datetime
    if output_type == 'pickle':
      # self.write_to_pickle()
      import pickle
      day = datetime.now().strftime('%y-%m-%d')
      f = open(os.path.join(output_dir, '%s_%d_specimens_%s.%s' % (day, self.N, self.data_type, output_type)), 'wb')
      pickle.dump(self, f)
      f.close()

    # TODO: create a tf.data.TFRecordDataset
    # elif output_type == 'tfrecord':
    #   self.write_to_tfrecord()
    #   pass
    else:
      raise NotImplementedError('Output type "%s" not supported.' % output_type)

  def write_to_tfrecord(self):
    pass



class DataReader:
  @classmethod
  def aggregate_data(cls, data_root_dir, data_type='auto_histogram', create_masks=False):
    """
    Aggregates raw image data including the specified img type, ct-scans, masks, the total number of specimens, the
    absolute specimen paths and the excitation frequencies which were used to acquire the phase images.
    :param str data_root_dir: path to the data root dir which contains a directory for each specimen
    :param str data_type: auto_histogram, full_scale or phase
    :param bool create_masks: whether to create masks based on auto histogram images and ct-scans
    :rtype: Data
    """

    assert os.path.isdir(data_root_dir), 'Data root dir "%s" does not exist' % data_root_dir
    assert data_type in gs.DATA_TYPES, 'Invalid image type'
    tmp_specimens, _ = get_sub_dirs_and_files(data_root_dir)
    cls.maybe_create_masks(data_root_dir, tmp_specimens, create_masks)
    num_specimens = len(tmp_specimens)
    assert num_specimens > 0, 'Data root directory "%s" is empty' % data_root_dir
    data = Data()
    specimen_names = []
    specimen_paths = []

    img_data = np.zeros((num_specimens, gs.H, gs.W) + (gs.C,), dtype=np.float)
    ct_scans = []

    auto_histogram_img_masks = np.zeros((num_specimens, gs.H, gs.W), dtype=np.uint8)
    ct_scan_masks = []

    # loop specimens
    cv2_read_flag = cv2.IMREAD_GRAYSCALE if data_type in ['auto_histogram', 'full_scale'] else cv2.IMREAD_UNCHANGED
    for n, specimen in enumerate(tmp_specimens):
      img_dir = os.path.join(data_root_dir, specimen, data_type)

      filenames = get_all_filenames(img_dir, gs.FILE_EXTS)
      freq_to_path, freqs = extract_exc_freqs(data_type, filenames, gs.EXC_FREQS, gs.IDX_TO_EXC_FREQ)
      # invalid specimen: either folder does not exist or the number of images is not equal to the number of excitation frequencies
      if not os.path.isdir(img_dir) or len(filenames) != gs.C:
        img_data = np.delete(img_data, n, 0)
        auto_histogram_img_masks = np.delete(auto_histogram_img_masks, n, 0)
        num_specimens -= 1
        n -= 1
        continue

      # found valid specimen
      # TODO: check if excitation frequencies, ct images and both masks are given in advance?
      if freqs == gs.EXC_FREQS:
        specimen_names.append(specimen)
        # store absolute specimen path
        specimen_path = os.path.join(data_root_dir, specimen)
        specimen_path_abs = os.path.abspath(specimen_path)
        specimen_paths.append(specimen_path_abs)

        img_stack = cls.read_phase_image_stack(freq_to_path, cv2_read_flag)
        img_data[n, :, :, :] = img_stack

        # read phase image mask
        fn = os.path.join(specimen_path, 'mask.png')
        assert os.path.isfile(fn), 'Mask does not exist: "%s"' % fn
        mask = cv2.imread(fn, cv2.IMREAD_GRAYSCALE)
        auto_histogram_img_masks[n] = mask

        # read CT images
        ct_dir = os.path.join(specimen_path, 'CT')
        ct_stack = cls.read_ct_image_stack(ct_dir)
        ct_scans.append(ct_stack)

        # read phase image mask
        fn = os.path.join(ct_dir, 'mask.png')
        assert os.path.isfile(fn), 'Mask does not exist: "%s"' % fn
        mask = cv2.imread(fn, cv2.IMREAD_GRAYSCALE)
        ct_scan_masks.append(mask)

    specimen_names = np.array(specimen_names)
    specimen_paths = np.array(specimen_paths)


    data.N = num_specimens
    data.specimen_names = specimen_names
    data.specimen_paths = specimen_paths
    data.data_type = data_type
    data.inputs = img_data
    data.ct_scans = ct_scans

    data.auto_histogram_img_masks = auto_histogram_img_masks
    data.ct_scan_masks = ct_scan_masks

    return data

  @staticmethod
  def read_phase_image_stack(freq_to_path, cv2_read_flag):
    """

    :param dict[float] freq_to_path: excitation frequency to image path
    :param int cv2_read_flag: cv2 image read flag
    :return:
    """
    img_stack = np.zeros((gs.H, gs.W) + (gs.C,), dtype=np.float)


    for idx, [freq, img_path] in enumerate(freq_to_path.items()):
      f = open(img_path, 'rb')
      b = bytearray(f.read())
      np_array = np.asarray(b, dtype=np.uint8)
      img = cv2.imdecode(np_array, cv2_read_flag)
      f.close()

      # rescale image
      if img.shape != gs.IMG_SHAPE:
        img = cv2.resize(img, gs.IMG_SHAPE, interpolation=cv2.INTER_CUBIC)

      img_stack[:, :, idx] = img

    return img_stack

  @staticmethod
  def read_ct_image_stack(ct_dir):
    ct_stack = []
    assert os.path.isdir(ct_dir), 'CT directory "%s" does not exist' % ct_dir
    ct_files = get_all_filenames(ct_dir, gs.FILE_EXTS, ignore_imgs_with='mask')
    assert len(ct_files) > 0, 'CT directory "%s" does not contain valid CT-scans' % ct_dir
    for ct_path in ct_files:
      ct_img = cv2.imread(ct_path, cv2.IMREAD_GRAYSCALE)
      ct_stack.append(ct_img)
    return np.stack(ct_stack)  # (C, H, W)
    # return np.stack(ct_stack, axis=2)  # (H, W, C)

  @staticmethod
  def maybe_create_masks(data_root_dir, specimens, create_masks=False):
    """
    Creates a phase image mask and a ct mask for each specimen.
    :param str data_root_dir:
    :param list specimens:
    :param bool create_masks:
    :return:
    """
    # assume that masks have been created manually
    if not create_masks:
      return

    specimen_paths = [os.path.join(data_root_dir, specimen) for specimen in specimens]

    for specimen_path in specimen_paths:
      # create phase image mask
      phase_img_mask_path = os.path.join(specimen_path, 'mask.png')
      # never overwrite existing masks
      if not os.path.isfile(phase_img_mask_path):
        logger.info('Creating binary phase image mask for "%s".' % specimen_path)
        auto_histogram_path = os.path.join(specimen_path, 'auto_histogram')
        auto_histogram_img_paths = glob.glob(auto_histogram_path + '/*')
        assert len(auto_histogram_img_paths) > 0, 'Binary mask does not exist and cannot be created because no auto' \
                                                  'histogram image is available (%s)' % auto_histogram_path
        create_phase_img_mask(auto_histogram_img_paths[0])

      # create ct mask
      ct_stack_path = os.path.join(specimen_path, 'CT')
      ct_mask_path = os.path.join(ct_stack_path, 'mask.png')
      if not os.path.isfile(ct_mask_path):
        logger.info('Creating binary CT mask for "%s".' % specimen_path)
        create_ct_mask(ct_stack_path)
